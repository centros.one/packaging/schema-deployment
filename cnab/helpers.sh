#!/usr/bin/env bash
set -euo pipefail

todo() {
  echo "TODO"
}

wait-for-registry-ready() {
  sleeptime=10
  if [[ $REGISTRY_READY_TIMEOUT > 0 ]]; then
    retries=$((REGISTRY_READY_TIMEOUT/sleeptime))
  else
    retries=-1
  fi
  namespace=${SCHEMAREGISTRY_NAMESPACE:-$NAMESPACE}

  echo "Schema registry pod selector: $SCHEMAREGISTRY_POD_SELECTOR / namespace: $namespace"

  while : ; do
    status=($(kubectl get pod --request-timeout="${KUBECTL_TIMEOUT}s" -n "$namespace" -l "$SCHEMAREGISTRY_POD_SELECTOR" \
      -o go-template='{{ if .items }}{{ range (index .items 0).status.conditions}}{{ if eq .type "Ready"}}{{.status}}{{"\t"}}{{ (index $.items 0).metadata.name }}{{end}}{{end}}{{end}}'))

    local ready=${status[0]:-False}
    pod_name=${status[1]:-}
    if [[ $ready == "True" ]] || [[ $retries == 0 ]]; then
      break # done
    else
      echo "No pod ready for selector '$SCHEMAREGISTRY_POD_SELECTOR' in namespace '$namespace', tries left: $([[ $retries > 0 ]] && echo $retries || echo '∞')"
    fi
    sleep $sleeptime
    if [[ $retries > 0 ]]; then
      ((retries--))
    fi
  done

  if [[ $retries == 0 ]]; then
    echo "Error: registry not ready, retries exhausted"
    exit 1
  fi
  echo "Pod '$pod_name' ready in namespace '$namespace'"
}

register-schema() {
  subjectname=$1
  schemadef=$2

  wait-for-registry-ready

  echo "Subject: $subjectname"
  echo "Schema:"
  echo "$schemadef" | jq .

  payload=$(echo -n "$schemadef" | jq -Mc '{schema: .|tostring}')
  result=$(kubectl exec -n "$namespace" "$pod_name" -c $SCHEMAREGISTRY_CONTAINER_NAME -- curl -s -X POST -H "Content-Type: application/vnd.schemaregistry.v1+json" \
    --data "$payload" "${SCHEMA_REGISTRY_URL%%*(/)}/subjects/$subjectname/versions")
  echo "Result: $result"
  errorcode=$(echo $result | jq '.["error_code"]')
  if [[ "$errorcode" != "null" ]]; then
    echo >&2 "Error $errorcode registering schema: $(echo $result | jq '.["message"]')"
    exit 1
  fi
}

# Call the requested function and pass the arguments as-is
"$@"
