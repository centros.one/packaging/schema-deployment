# Schema Deployment

A porter bundle for registering AVRO schemas in a kafka schema registry

## Parameters

Note that the ***name*** of the installation will be used as the ***subject*** on which the schema will be registered.

* `namespace`: the kubernetes namespace to operate in (optional, default: "`default`")
* `schema-registry-name`: the name of the schema registry cluster (required)
* `schema-definition`: the definition of the AVRO schema as a JSON string (required)
* `kubectl-timeout`: the time (in seconds) to wait for a `kubectl` operation to complete before considering it a failure (optional, default: `10`)
* `registry-ready-timeout`: the time to wait (in seconds) at most for the schema registry cluster to be ready before attempting to register the schema (optional, default: `600` = 10 minutes)
